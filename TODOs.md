# TODOs

- [X] Make rectangle move 1 direction	
- [X] Make rectangle alternate direction every few seconds.
- [X] Render game in 16:9 screen ratio all the time. All other screen screen ratios will display black borders
- [X] Full screen, screen ratio behaviour
- [ ] Introduce universal units that scale based on resolution (You maintain 16:9) so you need to ensure that your max unit width and max unit height are also have a 16:9 ratio respectively).
 - [ ] 90 units height, 160 units width. Go from there.
- [ ] Render ball
- [ ] Make ball move
- [ ] Add wall physics
- [ ] Create paddle
 - [ ] Paddle can move with keyboard 
