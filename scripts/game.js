// Note: Safari just doesn't support static fields. Static functions are fine
import Renderer from './renderer.js';
import MovingRectangleScene from './scenes/movingRectangle.js';

export default class Game {
	constructor() {
		this.stopMain = false;
		this.renderer = new Renderer();
		this.firstScene = new MovingRectangleScene();
	}

	render() {
		this.firstScene.render(this.renderer)
	}

	update(timestamp) {
		this.firstScene.update(timestamp);
	}
}

