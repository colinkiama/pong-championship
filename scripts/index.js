import Game from "./game.js";

let myGame = new Game();

(function () {
	function main( tFrame ) {
		myGame.stopMain = window.requestAnimationFrame( main );

		// Your main loop contents
		myGame.update( tFrame );

		myGame.render();
	}

	main(); // Start the cycle
})();