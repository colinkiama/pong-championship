import { setupScreen, calculateRenderArea, fetchSixteenByNineRatio } from './utils/graphics.js';

export default class Renderer {
	constructor() {
		let screen = setupScreen();
		this.context = screen.context; 
		this.canvas = screen.canvas; 

		this.recalculateRenderArea({
			width: this.canvas.width,
			height: this.canvas.height
		}, fetchSixteenByNineRatio());


		this.setupEvents();
	}

	setupEvents() {
		window.addEventListener('resize', (event) => {
			this.canvas.width =  window.innerWidth;
			this.canvas.height = window.innerHeight;

			this.recalculateRenderArea({
				width: this.canvas.width,
				height: this.canvas.height 
			}, fetchSixteenByNineRatio());
		});
	}

	recalculateRenderArea(dimensions, targetRatio) {
		this.renderArea = calculateRenderArea(dimensions, targetRatio);
	}

	fillRect(x, y, width, height) {
		this.context.fillRect(
			x + this.renderArea.x,
			y + this.renderArea.y,
			width,
			height
		);
	}

	clearScreen() {
		this.context.clearRect(
			0,
			0,
			this.canvas.width,
			this.canvas.height
		);

		this.context.fillStyle = 'red';

		this.context.fillRect(
			this.renderArea.x,
			this.renderArea.y,
			this.renderArea.width,
			this.renderArea.height
		)
	}

}