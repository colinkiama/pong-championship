export default class Scene {
	update(timestamp) {

	}

	render(renderer) {
		// You must clear the screen in order for canvas
		// animations to work
		renderer.clearScreen();
	}
}