import Scene from "../scene.js";
import { secondsToMilliseconds } from "../utils/time.js";

export default class MovingRectangleScene extends Scene {
	constructor(){
		super();
		
		this.alternationInterval = secondsToMilliseconds(3) ;
		this.lastTimestamp = -1;

		this.state = {
			// horizontal units per second
			acceleration: 100,
			timestampChecks: 0,
		};

		this.rectangle = {
			x: 10,
			y: 10,
			width: 150,
			height: 10
		};

		this.alternateAcceleration();
	}

	alternateAcceleration() {
		setTimeout(() => {
			this.state.acceleration = this.state.acceleration * -1;
			this.alternateAcceleration();
		}, this.alternationInterval);
	}

	update(timestamp) {
		super.update(timestamp);
		if (this.lastTimestamp > 0) {
			let delta = timestamp - this.lastTimestamp;
			let scale = delta / secondsToMilliseconds(1);
			this.rectangle.x = this.rectangle.x + this.state.acceleration * scale;
		}

		this.lastTimestamp = timestamp;
		this.state.timestampChecks++;
	}

	render(renderer)  {
		super.render(renderer);

		renderer.context.fillStyle = 'green';

		renderer.fillRect(
			this.rectangle.x,
			this.rectangle.y, 
			this.rectangle.width,
			this.rectangle.height
		);
	}
} 