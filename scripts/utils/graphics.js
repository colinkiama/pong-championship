function setupScreen() {
	let canvas = document.getElementById('main-game');
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	
	return {
		canvas: canvas,
		context: canvas.getContext('2d'),
	};
}

function calculateRenderArea(dimensions, targetRatio) {
	let inputRatio = dimensions.width / dimensions.height;
	let shouldScaleHeight = targetRatio > inputRatio;

	let renderWidth = shouldScaleHeight ? 
		dimensions.width :
 		dimensions.height * targetRatio;

	let renderHeight = shouldScaleHeight ?
		dimensions.width / targetRatio :
		dimensions.height;

	return {
		x: calculateRatioBoundsStart(dimensions.width, renderWidth),
		y: calculateRatioBoundsStart(dimensions.height, renderHeight),
		width: renderWidth,
		height: renderHeight,
	};
}

const fetchSixteenByNineRatio = () => 16 / 9;

const calculateRatioBoundsStart = (fullLength, otherLength) => (fullLength - otherLength) / 2;

export { setupScreen, calculateRenderArea, fetchSixteenByNineRatio };