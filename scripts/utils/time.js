import { SECONDS_IN_MILLISECONDS } from '../constants/time.js'; 

export const secondsToMilliseconds = (secondsToConvert) => secondsToConvert * SECONDS_IN_MILLISECONDS;

